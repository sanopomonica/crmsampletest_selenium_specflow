﻿using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Test.Acceptance.Infrastracture;
using Test.Acceptance.Library;
using Test.Acceptance.Library.Pages;


namespace Test.Acceptance.StepDefenition
{
    class BaseStepDefenition : Steps
    {
        protected BasePageModel basePageModel = new BasePageModel();

        protected Accounts accounts = new Accounts();
        protected CRMHome crmHome = new CRMHome();
        protected CRMSignup crmSignup = new CRMSignup();
        protected Leads leads = new Leads();
        protected NewAccount newAccount = new NewAccount();
        protected NewLead newLead = new NewLead();
        protected OpportunityForm opportunityForm = new OpportunityForm();

        public PooledItem<IWebDriver> PooledDriver
        {
            get
            {
                return ScenarioContext.Get<PooledItem<IWebDriver>>("PooledDriver");
            }
            set
            {
                ScenarioContext.Set(value, "PooledDriver");
            }
        }

        public ProxiedWebDriver Driver => new ProxiedWebDriver(PooledDriver.Value, ScenarioContext);

        [AfterScenario]
        public void DisposeWebDriver()
        {
            basePageModel.Dispose();
        }
    }
}
