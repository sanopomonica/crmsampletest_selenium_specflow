﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Test.Acceptance.StepDefenition
{
    [Binding]
    class DataCollectionStepDefinitions : BaseStepDefenition
    {
        [Given(@"I go to CRM login page")]
        public void GivenIGoToCRMLoginPage()
        {
            crmSignup.GoToURL("https://autotestcrm.crm.dynamics.com/main.aspx");
        }

        [Given(@"I have entered my '(.*)' and '(.*)'")]
        public void GivenIHaveEnteredMyUsernameAndPassword(string username, string password)
        {
            crmSignup.EnterCredentials(username, password);
        }

        [When(@"I press signin")]
        public void WhenIPressSignIn()
        {
            crmSignup.SignIn();
        }

        [Then(@"I should successfully go to CRM home page")]
        public void ThenIShouldSuccessfullyGoToCRMHomePage()
        {
            crmHome.ValidateSalesActivityDashboard();
        }

        [When(@"I click Sales Ribbon")]
        public void WhenIClickSalesRibbon()
        {
            crmHome.ClickSalesRibbon();
        }

        [When(@"I click Accounts menu")]
        public void WhenIClickAccountsMenu()
        {
            crmHome.ClickAccounts();
        }

        [Then(@"I will arrive in Active Accounts page")]
        public void ThenIWillArriveInActiveAccountsPage()
        {
            accounts.ValidateMyActiveAccounts();
        }

        [When(@"I click new button")]
        public void WhenIClickNewButton()
        {
            accounts.ClickNewAccounts();
        }

        [When(@"I populate the new account form with '(.*)'")]
        public void WhenIPopulateTheNewAccountForm(string accountname)
        {
            newAccount.EnterAccountDetails(accountname);
        }

        [When(@"I click Save button")]
        public void WhenIClickSaveButton()
        {
            newAccount.ClickSave();
        }

        [Then(@"I can see the newly created '(.*)' on the header")]
        public void ThenICanSeeTheNewlyCreatedAccountNameOnTheHeader(string name)
        {
            newAccount.ValidateNewlyCreatedUserNameDisplayedOnHeader(name);
        }

        [When(@"I close the new account page")]
        public void WhenICloseTheNewAccountPage()
        {
            newAccount.ClickClose();
        }

        [Then(@"I can see the newly created '(.*)' displayed in Account Name list")]
        public void ThenICanSeeTheNewlyCreatedUserDisplayedInAccountNameList(string name)
        {
            accounts.ValidateNewlyCreatedAccountFromTheList(name);
        }

        [When(@"I click Leads menu")]
        public void WhenIClickLeadsMenu()
        {
            crmHome.ClickLeads();
        }
        [Then(@"I will arrive in My Open Leads page")]
        public void ThenIWillArriveInMyOpenLeadsPage()
        {
            leads.ValidateMyOpenLeadsTitle();
        }

        [When(@"I click new leads button")]
        public void WhenIClickNewLeadsButton()
        {
            leads.ClickNewLeads();
        }

        [When(@"I populate the new leads form with '(.*)', '(.*)', '(.*)', '(.*)', etc.")]
        public void WhenIPopulateTheNewLeadsForm(string topic, string firstName, string lastName, string company)
        {
            newLead.PopulateNewLeadsForm(topic, firstName, lastName, company);
        }

        [When(@"I click Save button in New Lead form page")]
        public void WhenIClickSaveButtonInNewLeadFormPage()
        {
            newLead.ClickSave();
        }

        [Then(@"I can see the newly created '(.*) (.*)' on the header of new lead")]
        public void ThenICanSeeTheNewLeadIsCreated(string firstname, string lastname)
        {
            newLead.ValidateNewlyCreatedUserNameDisplayedOnHeader(firstname, lastname);
        }

        [Then(@"I will see that provided Description is displayed in Capture Summary field")]
        public void ThenIWillSeeThatCompanyIsDisplayedInCaptureSummaryField()
        {
            newLead.ValidateCaptureSummaryValue();
        }

        [When(@"I click Qualify")]
        public void WhenIClickQualify()
        {
            newLead.ClickQualify();
        }

        [Then(@"I can see header is changed from Lead to Opportunity")]
        public void ThenICanSeeHeaderIsChangedFromLeadsToOpportunity()
        {
            opportunityForm.ValidateHeaderChangedToOpportunity();
        }

        [Then(@"'(.*)' is displayed as the name header")]
        public void ThenTopicIsDisplayedAsTheNameHeader(string topic)
        {
            opportunityForm.ValidateNameHeader(topic);
        }

        [When(@"I go to next stage")]
        public void WhenIGoToNextStage()
        {
            opportunityForm.ClickNextStage();
        }

        [Then(@"I should see that Develop stage is completed")]
        public void ThenIShouldSeeThatDevelopStageIsCompleted()
        {
            opportunityForm.ValidateDevelopStageCompleted();
        }

        [Then(@"I should see that Propose stage is completed")]
        public void ThenIShouldSeeThatProposeStageIsCompleted()
        {
            opportunityForm.ValidateProposeCompleted();
        }

        [When(@"I go to finish stage")]
        public void WhenIGoToFinishStage()
        {
            opportunityForm.ClickFinishStage();
        }

        [Then(@"I should see that Close stage is completed")]
        public void ThenIShouldSeeThatCloseStageIsCompleted()
        {
            opportunityForm.ValidateCloseStageCompleted();
        }

        [When(@"I click close as won")]
        public void WhenIClickCloseAsWon()
        {
            opportunityForm.ClickMarkAsWon();
        }

        [When(@"I populate fields in the close opportunity popup")]
        public void WhenIPopulateFieldsInTheCloseOpportunityPopup()
        {
            opportunityForm.PopulateCloseOpportunity();
        }

        [When(@"click OK in the close opportunity popup")]
        public void WhenIClickOKInTheCloseOpportunityPopup()
        {
            opportunityForm.ClickOK();
        }

        [Then(@"validate that Reopened Opportunity button is displayed")]
        public void ThenValidateThatReopenedOpportunityButtonIsDisplayed()
        {
            opportunityForm.ValidateReopenedOpportunity();
        }

    }
}
