﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class Leads : BasePageModel
    {
        By NewAccounts = By.XPath("//li[contains(@title,'New')]//a");
        By MyOpenLeads = By.XPath(".//*[@id='crmGrid_SavedNewQuerySelector']/span[1]");
        By Overlay = By.Id("navBarOverlay");

        TimeSpan TimeSec = TimeSpan.FromSeconds(120);

        public void ClickNewLeads()
        {
            // Switch to Parent Frame
            GetParentFrameFocus();

            // Click the new account button
            System.Threading.Thread.Sleep(3000);
            Click(NewAccounts);
        }

        public void ValidateMyOpenLeadsTitle()
        {
            By frameVal = By.Id("contentIFrame0");
            var myActiveAccountsExpected = "My Open Leads";

            // Click the page overlay first
            Click(Overlay);

            WaitForElement(frameVal, TimeSec);
            GetFrameFocus(frameVal);
            Validation(MyOpenLeads, myActiveAccountsExpected);
        }
    }
}
