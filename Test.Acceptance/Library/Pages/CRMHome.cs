﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class CRMHome : BasePageModel
    {
        By Accounts = By.Id("nav_accts");
        By Leads = By.Id("nav_leads");
        By SalesActivityDashboard = By.Id("dashboardSelectorContainer");
        //By SalesActivityDashboard = By.CssSelector("#dashboardSelectorContainer");
        By SalesRibbon = By.CssSelector(".navBarTopLevelItem .navTabButtonArrowDown");
        By Trial = By.CssSelector(".lpath-gt-bubble-title-iconblock input");
        TimeSpan TimeSec = TimeSpan.FromSeconds(120);

        public void ClickAccounts()
        {
            WaitForElement(Accounts, TimeSec);
            Click(Accounts);
        }

        public void ClickLeads()
        {
            WaitForElement(Leads, TimeSec);
            Click(Leads);
        }

        public void ClickSalesRibbon()
        {
            GetParentFrameFocus();
            WaitForElement(SalesRibbon, TimeSec);
            Click(SalesRibbon);
        }

        public void ValidateSalesActivityDashboard()
        {
            By frameValue = By.Id("contentIFrame0");
            var salesActivityDashboardExpected = "Sales Activity Dashboard";
            
            if (IsElementPresent(Trial))
            {
                Click(Trial);
            }
            // click the trial version button
            //WaitForElement(Trial, TimeSec);
            //Click(Trial);

            // go to Dashboard frame
            GetFrameFocus(frameValue);

            // validate the home page dashboard logo
            WaitForElement(SalesActivityDashboard, TimeSec);
            Validation(SalesActivityDashboard, salesActivityDashboardExpected);
        }

    }
}
