﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class NewLead : BasePageModel
    {
        By Address = By.Id("Address_label");
        By AnnualRevenue = By.Id("Annual Revenue_label");
        By AnnualRevenueField = By.Id("revenue_i");
        By BusinessPhone = By.Id("Business Phone_label");
        By BusinessPhoneField = By.Id("telephone1_i");
        By Campaign = By.Id("campaignid_lookupValue");
        By CampaignField = By.Id("campaignid_ledit");
        By CaptureSummary = By.Id("Capture Summary_label");
        By City = By.Id("City_label");
        By CityField = By.Id("address1_composite_compositionLinkControl_address1_city_i");
        By Company = By.Id("Company_label");
        By CompanyField = By.Id("companyname_i");
        By ContactPreferred = By.Id("Preferred_label");
        By ContactPreferredField = By.Id("preferredcontactmethodcode_i");
        By Description = By.Id("Description_label");
        By DescriptionField = By.Id("description_i");
        By DoneButtton = By.Id("fullname_compositionLinkControl_flyoutLoadingArea-confirm");
        By DoneButtonCompany = By.Id("address1_composite_compositionLinkControl_flyoutLoadingArea-confirm");
        By Email = By.Id("Email_label");
        By EmailField = By.Id("emailaddress1_i");
        By FirstName = By.Id("fullname_compositionLinkControl_firstname_i");
        By Frame = By.Id("contentIFrame1");
        By Industry = By.Id("Industry_label");
        By IndustryField = By.Id("industrycode_i");
        By JobTitle = By.Id("Job Title_label");
        By JobTitleField = By.Id("jobtitle_i");
        By LastName = By.Id("Last Name_label");
        By LastNameField = By.Id("fullname_compositionLinkControl_lastname_i");
        By LeadNameHeader = By.CssSelector("#FormTitle h1");
        By MobilePhone = By.Id("Mobile Phone_label");
        By MobilePhoneField = By.Id("mobilephone_i");
        By Name = By.XPath(".//*[@id='fullname']/div[1]");
        By NextStage = By.XPath("//*[@id='stageNavigateActionContainer']/div");
        By NumOfEmployee = By.Id("No. of Employees_label");
        By NumOfEmployeeField = By.Id("numberofemployees_i");
        By Qualify = By.XPath("//li[@id='lead|NoRelationship|Form|Mscrm.Form.lead.ConvertLeadQuick']//span[@class='ms-crm-CommandBar-Menu']");
        By Save = By.Id("savefooter_statuscontrol");
        By SIC = By.Id("SIC Code_label");
        By SICField = By.Id("sic_i");
        By State = By.Id("State/Province_label");
        By StateField = By.Id("address1_composite_compositionLinkControl_address1_stateorprovince_i");
        By Street1 = By.Id("address1_composite_compositionLinkControl_address1_line1_i");
        By TitleFooter = By.Id("titlefooter_statuscontrol");
        By Topic = By.Id("Topic_label");
        By TopicField = By.Id("subject_i");
        By Website = By.Id("Website_label");
        By WebsiteField = By.Id("websiteurl_i");
        By ZipCode = By.Id("ZIP/Postal Code_label");
        By ZipCodeField = By.Id("address1_composite_compositionLinkControl_address1_postalcode_i");

        TimeSpan TimeSec = TimeSpan.FromSeconds(3000);

        // Test Data only
        string description = "Sample Company Only";

        public void ClickSave()
        {
            Click(Save);
        }

        public void ClickNextStage()
        {
            Click(NextStage);
        }

        public void ClickQualify()
        {
            GetParentFrameFocus();
            Click(Qualify);

            // Sleep
            System.Threading.Thread.Sleep(10000);
        }

        public void PopulateCompanyFields(string company)
        {
            string city = "Habbiton";
            string street1 = "112 George St.";
            string state = "Queensland";
            string website = "www.testwebsite.com";
            string zip = "1123";

            // Company
            ClickAndEnterValue(Company, CompanyField, company);

            // Company Website
            ClickAndEnterValue(Website, WebsiteField, website);

            // Address
            Click(Address);
            EnterValue(Street1, street1);
            ClickAndEnterValue(City, CityField, city);
            ClickAndEnterValue(State, StateField, state);
            ClickAndEnterValue(ZipCode, ZipCodeField, zip);
            Click(DoneButtonCompany);
        }

        public void PopulateContactFields(string topic, string firstName, string lastName)
        {
            string businessPhone = "0212345678";
            string email = $"{firstName}@test.com";
            string jobTitle = "Marketing";
            string mobilePhone = "0412345678";

            // Topic
            EnterValue(TopicField, topic);

            // Name
            Click(Name);
            EnterValue(FirstName, firstName);
            ClickAndEnterValue(LastName, LastNameField, lastName);
            Click(DoneButtton);

            // Job Title
            ClickAndEnterValue(JobTitle, JobTitleField, jobTitle);

            // Business Phone
            ClickAndEnterValue(BusinessPhone, BusinessPhoneField, businessPhone);

            // Mobile Phone
            ClickAndEnterValue(MobilePhone, MobilePhoneField, mobilePhone);

            // Email
            ClickAndEnterValue(Email, EmailField, email);
        }

        public void PopulateContactMethod()
        {
            string preferred = "Phone";

            // preferred
            ClickAndEnterValue(ContactPreferred, ContactPreferredField, preferred);
        } 

        public void PopulateDetails()
        {
            string annualRevenue = "10,000.00";
            string industry = "Transportation";
            string numOfEmployee = "200";
            string sic = "TEST";

            // Description
            ClickAndEnterValue(Description, DescriptionField, description);

            // Industry
            ClickAndSelect(Industry, IndustryField, industry);

            // Annual Revenue
            ClickAndEnterValue(AnnualRevenue, AnnualRevenueField, annualRevenue);

            // No. Of Employee
            ClickAndEnterValue(NumOfEmployee, NumOfEmployeeField, numOfEmployee);

            // SIC
            ClickAndEnterValue(SIC, SICField, sic);
        }

        public void PopulateMarketingInfo()
        {
            string campaign = "1Q Regional Events";

            // Source Campaign
            ClickAndEnterValue(Campaign, CampaignField, campaign);
        }

        public void PopulateNewLeadsForm(string topic, string firstName, string lastName, string company)
        {
            // Switch to iFrame
            GetFrameFocus(Frame);

            PopulateContactFields(topic, firstName, lastName);
            PopulateCompanyFields(company);
            PopulateDetails();
            PopulateMarketingInfo();
            PopulateContactMethod();
        }

        public void ValidateNewlyCreatedUserNameDisplayedOnHeader(string firstName, string lastName)
        {
            string expected = $"{firstName} {lastName}";
            string footerTitle = "saving";

            WaitForElementToBeInvisible(TitleFooter, footerTitle, TimeSec);
            Validation(LeadNameHeader, expected);
        }

        public void ValidateCaptureSummaryValue()
        {
            Validation(CaptureSummary, description);
        }

    }
}
