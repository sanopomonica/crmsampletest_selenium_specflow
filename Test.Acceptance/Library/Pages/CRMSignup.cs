﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class CRMSignup : BasePageModel
    {
        By Username = By.Id("cred_userid_inputtext");
        By Password = By.Id("cred_password_inputtext");
        By Signin = By.Id("cred_sign_in_button");

        public void EnterCredentials(string usernameValue, string passwordValue)
        {
            EnterValue(Username, usernameValue);
            EnterValue(Password, passwordValue);
        }

        public void SignIn()
        {
            System.Threading.Thread.Sleep(3000);
            Click(Signin);
        }

    }
}
