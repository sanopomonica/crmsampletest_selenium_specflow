﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class NewAccount :BasePageModel
    {
        By AccountName = By.Id("name_i");
        By AccountNameHeader = By.CssSelector("#FormTitle h1");
        By AccountNumber = By.XPath(".//*[@id='accountnumber']/div[1]");
        By AccountNumberField = By.Id("accountnumber_i");
        By AddressName = By.Id("Address Name_label");
        By AddressNameField = By.Id("address1_name_i");
        By AddressType = By.XPath(".//*[@id='address1_addresstypecode']/div[1]");
        By AddressTypeField = By.Id("address1_addresstypecode_i");
        By Close = By.Id("closeButton");
        By Email = By.XPath(".//*[@id='emailaddress1']/div[1]");
        By EmailField = By.Id("emailaddress1_i");
        By MainPhone = By.XPath(".//*[@id='telephone1']/div[1]");
        By MainPhoneField = By.Id("telephone1_i");
        By PrimaryContact = By.XPath(".//*[@id='primarycontactid']/div[1]");
        By PrimaryContactField = By.Id("primarycontactid_ledit");
        By Save = By.Id("savefooter_statuscontrol");
        By ShippingMethod = By.XPath(".//*[@id='address1_shippingmethodcode']/div[1]");
        By ShippingMethodField = By.Id("address1_shippingmethodcode_i");
        By TitleFooter = By.Id("titlefooter_statuscontrol");
        TimeSpan TimeSec = TimeSpan.FromSeconds(120);

        public void EnterAccountDetails(string name)
        {
            By frameVal = By.Id("contentIFrame1");
            string primaryContact = "Wilson Pais",
                accountNumber = "123456789",
                email = name + "@email.com",
                mainPhone = "123456789",
                addressType = "Bill To",
                addressName = "124 Habbiton",
                shipping = "DHL";
                

            // Switch to it's iframe
            WaitForElement(frameVal, TimeSec);
            GetFrameFocus(frameVal);

            // Populate the value
            // Account Name
            EnterValue(AccountName, name);

            // Primary Contact, field should be enabled first so it clicks the field first before entering
            Click(PrimaryContact);
            EnterValue(PrimaryContactField, primaryContact);

            // Account Number
            Click(AccountNumber);
            EnterValue(AccountNumberField, accountNumber);

            // Email
            Click(Email);
            EnterValue(EmailField, email);

            // Main Phone
            Click(MainPhone);
            EnterValue(MainPhoneField, mainPhone);

            // Address Type
            Click(AddressType);
            Dropdown(AddressTypeField, addressType);

            // Address Name
            Click(AddressName);
            System.Threading.Thread.Sleep(1000);
            //Click(AddressName);
            EnterValue(AddressNameField, addressName);

            // Shipping
            Click(ShippingMethod);
            Dropdown(ShippingMethodField, shipping);

        }

        public void ClickClose()
        {
            GetParentFrameFocus();
            Click(Close);
        }

        public void ClickSave()
        {
            Click(Save);
        }

        public void ValidateNewlyCreatedUserNameDisplayedOnHeader(string name)
        {
            string footerTitle = "saving";

            WaitForElementToBeInvisible(TitleFooter, footerTitle, TimeSec);
            Validation(AccountNameHeader, name);
        }
    }
}
