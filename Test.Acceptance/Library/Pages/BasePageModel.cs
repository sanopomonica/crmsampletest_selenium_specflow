﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using Test.Acceptance.Infrastracture;

namespace Test.Acceptance.Library.Pages
{
    public class BasePageModel
    {
        protected IWebDriver _driver = StaticDriver.driver;

        public void Click(By element)
        {
            FindElement(element).Click();
        }

        public void ClickClearAndEnterValue(By elementClick, By elementEnter, string value)
        {
            Click(elementClick);
            System.Threading.Thread.Sleep(1000);
            _driver.FindElement(elementEnter).Clear();
            EnterValue(elementEnter, value);
        }

        public void ClickAndEnterValue(By elementClick, By elementEnter, string value)
        {
            Click(elementClick);
            EnterValue(elementEnter, value);
        }

        public void ClickAndEnterValue(By elementClickEnter, string value)
        {
            Click(elementClickEnter);
            EnterValue(elementClickEnter, value);
        }

        public void ClickAndSelect(By elementClick, By elementEnter, string value)
        {
            Click(elementClick);
            Dropdown(elementEnter, value);
        }

        public void Dispose()
        {
            StaticDriver.driver.Quit();
            Console.Out.WriteLine("Completed Test");
        }

        public void Dropdown(By element, string val)
        {
            SelectElement oSelect = new SelectElement(FindElement(element));
            oSelect.SelectByText(val);
        }

        public void EnterValue(By element, string val)
        {
            FindElement(element).SendKeys(val);
        }

        protected IWebElement FindElement(By element)
        {
            return StaticDriver.driver.FindElement(element);
        }

        protected ReadOnlyCollection<IWebElement> FindElements(By element)
        {
            return StaticDriver.driver.FindElements(element);
        }

        public string GetAttribute(By element, string attribute)
        {
            IWebElement value = _driver.FindElement(element);
            string elementValue = value.GetAttribute(attribute);

            return elementValue;
        }

        public void GetFrameFocus(By frameVal)
        {
            StaticDriver.driver.SwitchTo().Frame(FindElement(frameVal));
        }

        public void GetParentFrameFocus()
        {
            StaticDriver.driver.SwitchTo().ParentFrame();
        }

        public void GoToURL(string URL)
        {
            StaticDriver.driver.Url = URL;
        }

        public bool IsElementPresent(By by)
        {
            try
            {
                _driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public void Validation(By elementActual, string valueExpected)
        {
            IWebElement newUserConfirmation = FindElement(elementActual);
            Assert.AreEqual(valueExpected, newUserConfirmation.Text, "Error: Expected and Actual are not equal");
        }

        public void Validation(string valueExpected, string valueActual)
        {
            Assert.AreEqual(valueExpected, valueActual, "Error: Expected and Actual are not equal");
        }

        public void WaitForElement(By val, TimeSpan timeout)
        {
            WebDriverWait wait = new WebDriverWait(StaticDriver.driver, timeout);
            IWebElement element = wait.Until(ExpectedConditions.ElementToBeClickable(val));
        }

        public void WaitForSpecificText(By element, string text, TimeSpan timeout)
        {

            WebDriverWait wait = new WebDriverWait(StaticDriver.driver, timeout);
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(element, text));
            //wait.Until(FindElement(element).Text == text);

        }

        public void WaitForElementToBeInvisible(By elementValue, string text, TimeSpan timeout)
        {
            WebDriverWait wait = new WebDriverWait(StaticDriver.driver, timeout);
            wait.Until(ExpectedConditions.InvisibilityOfElementWithText(elementValue, text));
        }
    }
}
