﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class Accounts : BasePageModel
    {
        By AccountPageWaitValueBeforeClicking = By.XPath("//div[@id='navBarOverlay' and contains(@style,'none')]");
        By MyActiveAccounts = By.XPath(".//*[@id='crmGrid_SavedNewQuerySelector']/span[1]");
        By AccountName = By.CssSelector("a[id^='gridBodyTable_primaryField']");
        By NewAccounts = By.XPath("//li[contains(@title,'New')]//a");
        By Overlay = By.Id("navBarOverlay");
        TimeSpan TimeSec = TimeSpan.FromSeconds(120);

        public void ClickNewAccounts()
        {
            // Switch to Parent Frame
            GetParentFrameFocus();

            // Click the new account button
            //WaitForElement(NewAccounts, TimeSec);
            System.Threading.Thread.Sleep(3000);
            Click(NewAccounts);
        }

        public void ValidateMyActiveAccounts()
        {
            By frameVal = By.Id("contentIFrame0");
            var myActiveAccountsExpected = "My Active Accounts";
            
            // Click the page overlay first
            // WaitForElement(Overlay, TimeSec);
            Click(Overlay);

            WaitForElement(frameVal, TimeSec);
            GetFrameFocus(frameVal);
            Validation(MyActiveAccounts, myActiveAccountsExpected);
        }

        public void ValidateNewlyCreatedAccountFromTheList(string newUserName)
        {
            
            By frameVal = By.Id("contentIFrame0");

            // Go to frame value then wait for the element to be visible
            GetFrameFocus(frameVal);
            WaitForElement(AccountName, TimeSec);

            // Check the array and assert if username is displayed from the list
            //var AccountNameArray = FindElements(AccountName);
            //var AccountNameList = AccountNameArray.ToList();
            //foreach (var accountName in AccountNameArray)
            //{
            //    string result = accountName.Text;
            //    if(result == newUserName)
            //    {
            //        Assert.AreEqual(newUserName, accountName);
            //        Console.WriteLine(result + " is displayed at the list");
            //    }
            //    else
            //    {
            //        Assert.Fail("element not found");
            //    }
            //}
            //string AccountNameList;
            //foreach (var accountName in AccountNameArray)
            //{
            //    string result = accountName.Text;
            //    AccountNameList = result + ", ";
            //}

            IList<IWebElement> accountNames = FindElements(AccountName);
            String[] accountNameList = new String[accountNames.Count];
            int i = 0;
            foreach (IWebElement element in accountNames)
            {
                accountNameList[i++] = element.Text;
            }

            Assert.Contains(newUserName, accountNameList);

        }
    }
}
