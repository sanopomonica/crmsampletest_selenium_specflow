﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class OpportunityForm : BasePageModel
    {
        By ActualRevenue = By.Id("Actual Revenue_label");
        By ActualRevenueField = By.Id("actualrevenue_id_i");
        By CloseAsWon = By.XPath(".//*[@id='opportunity|NoRelationship|Form|Mscrm.Form.opportunity.MarkAsWon']//span[@class='ms-crm-CommandBar-Menu']");
        By CloseStageTitle = By.CssSelector("#stage_3 .processStageTailContainer");
        By CloseOpportunityOK = By.Id("ok_id");
        By Description = By.Id("Description_label");
        By DescriptionField = By.Id("description_id_i");
        By DetailedAmount = By.CssSelector("#description_id ");
        By DevelopStageTitle = By.CssSelector("#stage_1 .processStageTailContainer");
        By FormHeader = By.CssSelector("#formselectorcontainer span");
        By Header = By.CssSelector("#Tabnav_oppts-main .navTabButtonText");
        By Frame = By.Id("contentIFrame1");
        By FrameCloseOpportunity = By.Id("InlineDialog_Iframe");
        By FinishStage = By.CssSelector("#stageFinishActionContainer .stageActionTextForIcon");
        By NextStage = By.CssSelector("#stageAdvanceActionContainer .stageActionTextForIcon");
        By ProposeStageTitle = By.CssSelector("#stage_2 .processStageTailContainer");
        By ReopenedOpportunityButton = By.CssSelector(".ms-crm-CommandBar-Button span");
        By TopicHeader = By.CssSelector("#FormTitle h1");

        TimeSpan TimeSec = TimeSpan.FromSeconds(10000);

        public void ClickNextStage()
        {
            Click(NextStage);

            // Sleep
            System.Threading.Thread.Sleep(5000);
        }

        public void ClickFinishStage()
        {
            Click(FinishStage);

            // Sleep
            System.Threading.Thread.Sleep(5000);
        }

        public void ClickMarkAsWon()
        {
            GetParentFrameFocus();

            Click(CloseAsWon);
        }

        public void ClickOK()
        {
            Click(CloseOpportunityOK);
        }

        public void PopulateCloseOpportunity()
        {
            string actualRevenueVal = "10,000,000.00";
            string description = "Opportunity won - for test demo only";

            GetFrameFocus(FrameCloseOpportunity);
            ClickClearAndEnterValue(ActualRevenue, ActualRevenueField, actualRevenueVal);
            ClickAndEnterValue(Description, DescriptionField, description);
        }

        public void PopulateQuotation()
        {
            // Detailed Amount

            // Discount

            // Pre-Freight Amount

        }

        public void ValidateHeaderChangedToOpportunity()
        {
            string expected = "OPPORTUNITY";
            string headerText = "Opportunities";

            GetParentFrameFocus();

            // wait to change the header
            WaitForSpecificText(Header, headerText, TimeSec);

            // switch to iframe
            GetFrameFocus(Frame);

            WaitForElement(FormHeader, TimeSec);
            Validation(FormHeader, expected);
        }

        public void ValidateCloseStageCompleted()
        {
            string attribute = "title";
            string expected = "Entity: Opportunity, Stage: Close, Status: Completed";
            string actual = GetAttribute(CloseStageTitle, attribute);

            Validation(expected, actual);
        }

        public void ValidateDevelopStageCompleted()
        {
            string attribute = "title";
            string expected = "Entity: Opportunity, Stage: Develop, Status: Completed";
            string actual = GetAttribute(DevelopStageTitle, attribute);

            Validation(expected, actual);
        }

        public void ValidateNameHeader(string topic)
        {
            Validation(TopicHeader, topic);
        }

        public void ValidateProposeCompleted()
        {
            string attribute = "title";
            string expected = "Entity: Opportunity, Stage: Propose, Status: Completed";
            string actual = GetAttribute(ProposeStageTitle, attribute);

            Validation(expected, actual);
        }

        public void ValidateReopenedOpportunity()
        {
            string expected = "REOPEN OPPORTUNITY";

            System.Threading.Thread.Sleep(9000);
            GetParentFrameFocus();
            Validation(ReopenedOpportunityButton, expected);
        }
    }
}
