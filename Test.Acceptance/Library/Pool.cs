﻿using System;
using System.Collections.Generic;
using System.Linq;
using ThirdDrawer.Extensions.CollectionExtensionMethods;

namespace Test.Acceptance.Library
{
    public class Pool<T> : IDisposable where T : IDisposable
    {
        private readonly Func<T> itemFactory;
        private readonly Action<T> itemReset;
        private readonly Stack<T> items = new Stack<T>();
        private readonly object mutex = new object();

        public Pool(Func<T> itemFactory, Action<T> itemReset)
        {
            this.itemFactory = itemFactory;
            this.itemReset = itemReset;
        }

        public PooledItem<T> BorrowItem()
        {
            lock (mutex)
            {
                if (items.Any())
                {
                    var item = items.Pop();
                    return new PooledItem<T>(item, this);
                }
            }

            return new PooledItem<T>(itemFactory(), this);
        }

        public void ReturnItem(T item)
        {
            lock (mutex)
            {
                itemReset(item);
                items.Push(item);
            }
        }

        public void Dispose()
        {
            items
                .Do(item => item.Dispose())
                .Done();
        }
    }
}
