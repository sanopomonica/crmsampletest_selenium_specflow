﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library
{
    public class PooledItem<T> : IDisposable where T : IDisposable
    {
        private readonly T item;
        private readonly Pool<T> pool;

        public PooledItem(T item, Pool<T> pool)
        {
            this.item = item;
            this.pool = pool;
        }

        public T Value { get { return item; } }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            pool.ReturnItem(item);
        }

        public static implicit operator T(PooledItem<T> item)
        {
            return item.Value;
        }
    }
}
