﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;

namespace Test.Acceptance.Infrastracture
{
    public static class StaticDriver
    {
        private static IWebDriver _driver;

        public static IWebDriver driver
        {
            get
            {
                if (_driver == null)
                {
                    var options = new ChromeOptions();
                    options.AddArguments("chrome.switches", "--disable-extensions --disable-extensions-file-access-check --disable-extensions-http-throttling --disable-infobars --enable-automation --start-maximized");
                    options.AddUserProfilePreference("credentials_enable_service", false);
                    options.AddUserProfilePreference("profile.password_manager_enabled", false);

                    _driver = new ChromeDriver(options);
                }
                return _driver;
            }
        }

        public static IWebDriver GetDriver()
        {
            return driver;
        }
    }
}
