﻿using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace Test.Acceptance.Infrastracture
{
    public class ProxiedWebDriver : IJavaScriptExecutor, IHasInputDevices
    {
        public IWebDriver ProxiedDriver { get; }
        private readonly ScenarioContext scenarioContext;
        private readonly IJavaScriptExecutor javaScriptExecutorImplementation;
        private readonly IHasInputDevices hasInputDevicesImplementation;

        public ProxiedWebDriver(IWebDriver proxiedDriver, ScenarioContext scenarioContext)
        {
            ProxiedDriver = proxiedDriver;
            javaScriptExecutorImplementation = (IJavaScriptExecutor)proxiedDriver;
            hasInputDevicesImplementation = (IHasInputDevices)proxiedDriver;
            this.scenarioContext = scenarioContext;
        }

        public IKeyboard Keyboard => hasInputDevicesImplementation.Keyboard;

        public IMouse Mouse => hasInputDevicesImplementation.Mouse;

        public object ExecuteAsyncScript(string script, params object[] args)
        {
            return javaScriptExecutorImplementation.ExecuteAsyncScript(script, args);
        }

        internal IWebElement FindElement(string element)
        {
            throw new NotImplementedException();
        }

        public object ExecuteScript(string script, params object[] args)
        {
            return javaScriptExecutorImplementation.ExecuteScript(script, args);
        }

        public void GoToURL(string url)
        {
            ProxiedDriver.Url = url;
        }

        public void Close()
        {
            ProxiedDriver.Close();
        }

        public void Dispose()
        {
            ProxiedDriver.Dispose();
        }

        public IWebElement FindElement(By by)
        {
            return ProxiedDriver.FindElement(by);
        }
    }
}
