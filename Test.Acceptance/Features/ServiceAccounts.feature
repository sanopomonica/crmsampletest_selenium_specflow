﻿Feature: ServiceAccounts

Scenario Outline: Successfully Create A New Account
	Given I go to CRM login page
	And I have entered my '<username>' and '<password>'
	When I press signin
	Then I should successfully go to CRM home page
	When I click Sales Ribbon
	And I click Accounts menu
	Then I will arrive in Active Accounts page
	When I click new button
	And I populate the new account form with '<account name>'
	And I click Save button
	Then I can see the newly created '<account name>' on the header
	When I close the new account page
	Then I can see the newly created '<account name>' displayed in Account Name list
	
Examples: 
| username                          | password  | account name |
| admin@autotestcrm.onmicrosoft.com | p@ss1word | tester30     | 

Scenario Outline: Create Lead and Opportunity for Won Account
	Given I go to CRM login page
	And I have entered my '<username>' and '<password>'
	When I press signin
	Then I should successfully go to CRM home page
	When I click Sales Ribbon
	And I click Leads menu
	Then I will arrive in My Open Leads page
	When I click new leads button
	And I populate the new leads form with '<topic>', '<first name>', '<last name>', '<company>', etc.
	And I click Save button in New Lead form page
	Then I can see the newly created '<first name> <last name>' on the header of new lead
	And I will see that provided Description is displayed in Capture Summary field
	When I click Qualify
	Then I can see header is changed from Lead to Opportunity
	And '<topic>' is displayed as the name header
	When I go to next stage
	Then I should see that Develop stage is completed
	When I go to next stage
	Then I should see that Propose stage is completed 
	When I go to finish stage
	Then I should see that Close stage is completed
	When I click close as won
	And I populate fields in the close opportunity popup
	And click OK in the close opportunity popup
	Then validate that Reopened Opportunity button is displayed

Examples: 
| username                          | password  | topic								| first name | last name | company						|
| admin@autotestcrm.onmicrosoft.com | p@ss1word | Test Automation Project Topic01	| Bilbo      | Baggins   | Test Automation Company01	|
| admin@autotestcrm.onmicrosoft.com | p@ss1word | Test Automation Project Topic02	| Bilbo      | Baggins   | Test Automation Company02	|
| admin@autotestcrm.onmicrosoft.com | p@ss1word | Test Automation Project Topic03	| Bilbo      | Baggins   | Test Automation Company03	|
